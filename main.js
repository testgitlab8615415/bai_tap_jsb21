var dssv=[];
dataJson=localStorage.getItem("DSSV");
if(dataJson!=null){
    dssv=JSON.parse(dataJson).map(function (item){
        return new SinhVien(
            item.ma,
            item.name,
            item.email,
            item.password,
            item.math,
            item.phisic,
            item.chemistry,
        );
    });
    renderDSSV(dssv);
}
function addSinhVien(){
    var sv=loadDataFromForm();
    dssv.push(sv);
    renderDSSV(dssv);
    var dataJson=JSON.stringify(dssv);
    localStorage.setItem('DSSV',dataJson);
}
function xoaSinhVien(maSv){
    // index sẽ chạy từ 0-->dssv.length 
    var index=dssv.findIndex(function(intt){
        
        return (maSv == intt.ma);
    });
    dssv.splice(index,1);
    
    // console.log(dssv.maSV);
    //    for(var i=0 ; i<dssv.length;i++){
    //  if(maSv == dssv[i].ma){
    //   dssv.splice(i,1);
    // }};
    renderDSSV(dssv);
    var dataJson=JSON.stringify(dssv);
    localStorage.setItem('DSSV',dataJson);
}
function suaSinhVien(maSv){
    var index=dssv.findIndex(function(intt){
        
        return (maSv == intt.ma);
    });
    nv=dssv[index];
    show=showInfoToForm(nv);
}
function capNhatSinhVien(){
    for(var i=0; i<dssv.length;i++){
      var sv=loadDataFromForm();
    if(sv.ma == dssv[i].ma){
        dssv[i]=sv;
        console.log(dssv);
    }
  }
  document.getElementById("formQLSV").reset();
  renderDSSV(dssv);
  var dataJson=JSON.stringify(dssv);
    localStorage.setItem('DSSV',dataJson);
}

function resetForm(){
    document.getElementById("formQLSV").reset();
}